@include('templates.header', ['status' => 'complete'])

<div class="container">
    <table class="table table-striped" id="table">
        <thead>
            <tr>
                <th width="30px">#</th>
                <th>Name</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody id="interior">
            @php
                $count = 1
            @endphp
            @foreach ($items as $item)
                <tr>
                    <td class="pl-3">
                        {{$count++}}
                    </td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->price}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a class="btn btn-success mt-3" href="{{route('create.item', $category_id)}}">Create Item</a>
</div>

@include('templates.footer', ['status' => 'complete'])
