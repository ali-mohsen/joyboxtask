@include('templates.header', ['status' => 'complete'])

<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card">
            <h3 class="card-header text-center">Create item</h3>
            <div class="card-body">
                <form action="{{route('create.item', $category->id)}}" method="post">
                    @csrf
                    <div class="form-group mb-3">
                        <input type="text" placeholder="Name" id="name" class="form-control" name="name" required
                            autofocus>
                        @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    <div class="form-group mb-3">
                        <input type="number" min="0.01" step="0.01" placeholder="Price" id="price" class="form-control" name="price" required>
                        @if ($errors->has('price'))
                        <span class="text-danger">{{ $errors->first('price') }}</span>
                        @endif
                    </div>

                    <div class="d-grid mx-auto">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('templates.footer', ['status' => 'complete'])
