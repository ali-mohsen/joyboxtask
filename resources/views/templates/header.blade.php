<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>

    <link href="{{url('static/bootstrap-5.0.2-dist/css/bootstrap.min.css')}}" rel="stylesheet"></head>
    <script src="{{url('static/js/jquery-3.6.0.min.js')}}"></script>
    
<body>

    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light min-vh-100 position-fixed">
                <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100"> {{-- position-fixed --}}
                    <a href="/" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline">Menu</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                        <li>
                            <a href="#First-task" data-bs-toggle="collapse" class="nav-link px-0 align-middle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-1-square" viewBox="0 0 16 16">
                                    <path d="M9.283 4.002V12H7.971V5.338h-.065L6.072 6.656V5.385l1.899-1.383z"/>
                                    <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm15 0a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1z"/>
                                </svg>
                                <span class="ms-1 d-none d-sm-inline">First Task</span>
                            </a>
                            <ul class="collapse nav flex-column ms-1" id="First-task" data-bs-parent="#menu">
                                <li class="w-100">
                                    <a href="{{route('show.categories')}}" class="nav-link px-0"> <span class="d-none d-sm-inline">Show Categories</span></a>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="#Second-task" data-bs-toggle="collapse" class="nav-link px-0 align-middle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-2-square" viewBox="0 0 16 16">
                                    <path d="M6.646 6.24v.07H5.375v-.064c0-1.213.879-2.402 2.637-2.402 1.582 0 2.613.949 2.613 2.215 0 1.002-.6 1.667-1.287 2.43l-.096.107-1.974 2.22v.077h3.498V12H5.422v-.832l2.97-3.293c.434-.475.903-1.008.903-1.705 0-.744-.557-1.236-1.313-1.236-.843 0-1.336.615-1.336 1.306"/>
                                    <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm15 0a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1z"/>
                                </svg>
                                <span class="ms-1 d-none d-sm-inline">Second Task</span>
                            </a>
                            <ul class="collapse nav flex-column ms-1" id="Second-task" data-bs-parent="#menu">
                                <li class="w-100">
                                    <a href="{{route('show.quizzes')}}" class="nav-link px-0"> <span class="d-none d-sm-inline">Show Quizzes</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <button onclick="startFCM()" class="btn btn-primary btn-flat d-none d-sm-block">
                        Allow notification
                    </button>
                    <hr>
                </div>
            </div>

            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light min-vh-100"></div>

            <div class="col py-3">
