<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>

    <link href="{{url('static/bootstrap-5.0.2-dist/css/bootstrap.min.css')}}" rel="stylesheet"></head>
    <script src="{{url('static/js/jquery-3.6.0.min.js')}}"></script>
<body>