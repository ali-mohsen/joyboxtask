@include('templates.header', ['status' => 'complete'])

<div class="container">
    <table class="table table-striped" id="table">
        <thead>
            <tr>
                <th width="30px">#</th>
                <th>Name</th>
                <th data-orderable="false">Actions</th>
            </tr>
        </thead>
        <tbody id="interior">
            @php
                $count = 1
            @endphp
            @foreach ($quizzes as $quiz)
                <tr>
                    <td class="pl-3">
                        {{$count++}}
                    </td>
                    <td>{{$quiz->name}}</td>
                    <td>
                        <a class="btn btn-dark" href="{{route('add.result', $quiz->id)}}">
                            Add Result
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a class="btn btn-success mt-3" href="{{route('create.quiz')}}">Create Quiz</a>
</div>

@include('templates.footer', ['status' => 'complete'])
