@include('templates.header', ['status' => 'complete'])

<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="card">
            <h3 class="card-header text-center">Create Quiz</h3>
            <div class="card-body">
                <form action="{{route('create.quiz')}}" method="post">
                    @csrf
                    <div class="form-group mb-3">
                        <input type="text" placeholder="Quiz Name" id="name" class="form-control" name="name"
                            autofocus>
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    <h3 class="text-center">Question 1</h3>
                    <div class="form-group mb-3">
                        <textarea placeholder="Question Text" class="form-control" name="questions[0][text]"></textarea>
                        @if ($errors->has('questions.0.text'))
                            <span class="text-danger">{{ $errors->first('questions.0.text') }}</span>
                        @endif
                    </div>

                    <h5 class="text-center">Answer 1</h5>
                    <div class="form-group mb-3">
                        <textarea placeholder="Answer Text" class="form-control" name="questions[0][answers][0][text]"></textarea>
                        @if ($errors->has('questions.0.answers.0.text'))
                            <span class="text-danger">{{ $errors->first('questions.0.answers.0.text') }}</span>
                        @endif
                    </div>

                    <div class="form-group mb-3">
                        <input placeholder="Answer grade" class="form-control" type="number" name="questions[0][answers][0][grade]">
                        @if ($errors->has('questions.0.answers.0.grade'))
                            <span class="text-danger">{{ $errors->first('questions.0.answers.0.grade') }}</span>
                        @endif
                    </div>

                    <div class="form-group mb-3">
                        @if ($errors->has('questions.0.answers'))
                            <span class="text-danger">{{ $errors->first('questions.0.answers') }}</span>
                        @endif
                    </div>

                    <button type="button" class="moreAnswers btn btn-success btn-block" data-question="0">
                        add answer
                    </button>
                    <hr>


                    <button type="button" class="btn btn-success btn-block" id="moreQuestions">
                        Add Question
                    </button>
                    <div class="form-group mb-3">
                        @if ($errors->has('questions'))
                            <span class="text-danger">{{ $errors->first('questions') }}</span>
                        @endif
                    </div>

                    <div class="d-grid mx-auto">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    let questionCount = 0;
    let answersCount = [];
    answersCount.push(0)
    $('#moreQuestions').on('click', function(e){
        e.preventDefault();
        questionCount++;
        $(this).before(`
            <h3 class="text-center">Question ${questionCount + 1}</h3>
            <div class="form-group mb-3">
                <textarea placeholder="Question Text" class="form-control" name="questions[${questionCount}][text]"></textarea>
            </div>

            <h5 class="text-center">Answer 1</h5>
            <div class="form-group mb-3">
                <textarea placeholder="Answer Text" class="form-control" name="questions[${questionCount}][answers][0][text]"></textarea>
            </div>
            <div class="form-group mb-3">
                <input placeholder="Answer grade" class="form-control" type="number" name="questions[${questionCount}][answers][0][grade]">
            </div>

            <button type="button" class="moreAnswers btn btn-success btn-block" data-question="${questionCount}">
                add answer
            </button>
            <hr>
        `);
        answersCount.push(0)
    });

    $(document).on('click', '.moreAnswers', function () {
        const thisQuestion = $(this).data('question');
        if(answersCount[thisQuestion] >= 4){
            return;
        }
        const count = ++answersCount[thisQuestion];
        $(this).before(`
            <h5 class="text-center">Answer ${count + 1}</h5>
            <div class="form-group mb-3">
                <textarea placeholder="Answer Text" class="form-control" name="questions[${thisQuestion}][answers][${count}][text]"></textarea>
            </div>
            <div class="form-group mb-3">
                <input placeholder="Answer grade" class="form-control" type="number" name="questions[${thisQuestion}][answers][${count}][grade]">
            </div>
        `);
    });
</script>

@include('templates.footer', ['status' => 'complete'])
