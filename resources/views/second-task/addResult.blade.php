@include('templates.header', ['status' => 'complete'])

<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card">
            <h3 class="card-header text-center">{{$quiz->name}} Results</h3>
            <div class="card-body">
                <form action="{{route('add.result', $quiz->id)}}" method="post">
                    @csrf
                    <div class="form-group mb-3">
                        <input type="number" min="0" placeholder="min grade" class="form-control" name="results[0][min_grade]" value="{{old('results.0.min_grade')}}">
                        @if ($errors->has('results.0.min_grade'))
                        <span class="text-danger">{{ $errors->first('results.0.min_grade') }}</span>
                        @endif
                    </div>

                    <div class="form-group mb-3">
                        <input type="number" min="0" placeholder="max grade" class="form-control" name="results[0][max_grade]" value="{{old('results.0.max_grade')}}">
                        @if ($errors->has('results.0.max_grade'))
                        <span class="text-danger">{{ $errors->first('results.0.max_grade') }}</span>
                        @endif
                    </div>

                    <div class="form-group mb-3">
                        <textarea placeholder="Evaluation" class="form-control" name="results[0][evaluation]">{{old('evaluation')}}</textarea>
                        @if ($errors->has('results.0.evaluation'))
                            <span class="text-danger">{{ $errors->first('results.0.evaluation') }}</span>
                        @endif
                    </div>
                    <hr>

                    <button type="button" class="btn float-right mt-1" id="moreResults">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                        </svg>
                    </button>
                    
                    @if ($errors->has('results'))
                        <div class="form-group mb-3">
                            <span class="text-danger">{{ $errors->first('results') }}</span>
                        </div>
                    @endif

                    <div class="d-grid mx-auto">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    let resultCount = 0;
    $('#moreResults').on('click', function(e){
        e.preventDefault();
        resultCount++;
        $(this).before(`
            <div class="form-group mb-3">
                <input type="number" min="0" placeholder="min grade" class="form-control" name="results[${resultCount}][min_grade]">
            </div>

            <div class="form-group mb-3">
                <input type="number" min="0" placeholder="max grade" class="form-control" name="results[${resultCount}][max_grade]">
            </div>

            <div class="form-group mb-3">
                <textarea placeholder="Evaluation" class="form-control" name="results[${resultCount}][evaluation]"></textarea>
            </div>
            <hr>
        `);
    });
</script>
@include('templates.footer', ['status' => 'complete'])
