<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class Range implements ValidationRule
{

    protected $start;
    protected $end;

    /**
     * Create a new rule instance.
     *
     * @param int $start
     * @param int $end
     */
    public function __construct(int $start, int $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $temp = $this->start;
        for($i=0;$i<count($value); $i++){
            if($value[$i]['min_grade'] > $value[$i]['max_grade']){
                $fail('The min grade field must be less than or equal to max grade.');
            }
            $temp += $value[$i]['max_grade'] - $value[$i]['min_grade'];
            for($j=$i+1;$j<count($value); $j++){
                if($value[$i]['min_grade'] <= $value[$j]['max_grade'] && $value[$i]['max_grade'] >= $value[$j]['min_grade'])
                {
                    $fail('The Ranges intersect each other.');
                }
            }
        }
        $temp += count($value) - 1;
        if($temp != $this->end){
            $fail('There is a missing range.');
        }
    }
}
