<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Quiz extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'min_grade',
        'max_grade',
    ];

    public function results() : HasMany
    {
        return $this->hasMany(Result::class);
    }
    
    public function questions() : HasMany
    {
        return $this->hasMany(Question::class);
    }
}
