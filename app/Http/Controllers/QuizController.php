<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Quiz;
use App\Models\Result;
use App\Rules\Range;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function showQuizzes()
    {
        $quizzes = Quiz::all();
        return view('second-task.quizzes', ['quizzes' => $quizzes]);
    }

    public function createQuiz(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'name' => 'required|string',
                "questions" => "required|array|min:1",
                "questions.*.text" => "required|string",
                "questions.*.answers" => "required|array|min:2|max:5",
                "questions.*.answers.*.text" => "required|string",
                "questions.*.answers.*.grade" => "required|numeric|min:0",
            ], [
                "questions.min" => 'Quizzes must have at least 1 question.',
                "questions.*.answers.min" => 'Questions must have at least 2 answers.',
                "questions.*.answers.max" => 'Questions must have at most 5 answers.',
                "questions.*.text.required" => 'The text field is required.',
                "questions.*.answers.*.text" => 'The text field is required.',
                "questions.*.answers.*.grade" => 'The grade field is required.',
            ]);

            $min_grade = 0; // مجموع اقل علامة من كل سؤال
            $max_grade = 0; // مجموع اكبر علامة من كل سؤال

            $quiz = Quiz::create([
                'name' => $request->input('name'),
                'min_grade' => $min_grade,
                'max_grade' => $max_grade,
            ]);
            
            $questions = $request->input('questions');
            foreach ($questions as $question) {
                $min = PHP_INT_MAX;
                $max = 0;
                $q = Question::create([
                    'quiz_id' => $quiz->id,
                    'text' => $question['text'],
                ]);
                foreach ($question['answers'] as $answer) {
                    if ($answer['grade'] < $min){
                        $min = $answer['grade'];
                    }
                    if ($answer['grade'] > $max){
                        $max = $answer['grade'];
                    }
                    Answer::create([
                        'question_id' => $q->id,
                        'text' => $answer['text'],
                        'grade' => $answer['grade'],
                    ]);
                }
                $min_grade += $min;
                $max_grade += $max;
            }

            $quiz->update([
                'min_grade' => $min_grade,
                'max_grade' => $max_grade,
            ]);

            return redirect(route('show.quizzes'));
        }
        return view('second-task.createQuiz');
    }

    public function addResult(Request $request, Quiz $quiz)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'results' => ['required', 'array', 'min:2', new Range($quiz->min_grade, $quiz->max_grade)],

                "results.*.min_grade" => "required|numeric|min:$quiz->min_grade",
                "results.*.max_grade" => "required|numeric|max:$quiz->max_grade",
                "results.*.evaluation" => "required|string",
            ],[
                'results.min' => 'Quizzes must have at least 2 results.',
            ]);

            $results = $request->input('results');
            foreach ($results as $key => $result) {
                $results[$key]['quiz_id'] = $quiz->id;
            }
            Result::insert($results);
        }
        
        return view('second-task.addResult', ['quiz' => $quiz, 'results' => $quiz->results]);
    }
}
