<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;

// first task...

class ItemController extends Controller
{
    public function showcategories()
    {
        $categories = Category::all();
        return view('first-task.categories', ['categories' => $categories]);
    }

    public function showCategoryItems(Category $category)
    {
        $items = $category->items;
        return view('first-task.items', ['items' => $items, 'category_id' => $category->id]);
    }

    // store item and send notification
    public function createItem(Request $request, Category $category)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'name' => 'required',
                'price' => 'required|numeric|min:0',
            ]);

            /** @var Item */
            $item = Item::create([
                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'category_id' => $category->id,
            ]);

            $this->_sendNotificationToAllUsers($item);

            return redirect(route('show.category-items', $category->id));
        }
        return View('first-task.createItem', ['category' => $category]);
    }
  

  
    private function _sendNotificationToAllUsers(Item $item) : bool
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $FcmToken = User::whereNotNull('fcm_key')->pluck('fcm_key')->all();
        
        $headers = [
            'Authorization' => 'key=' . env('FCM_TOKEN'),
            'Content-Type' => 'application/json',
        ];
        $data = [
            "registration_ids" => $FcmToken,
            "notification" => [
                "title" => 'New Item had been added',
                "body" => "Name: $item->name, Price: $item->price",
            ]
        ];

        $client = new \GuzzleHttp\Client();
        try {
            $result = $client->request('POST', $url, [
                'headers' => $headers,
                'body' => json_encode($data)
            ]);
        } catch (\Throwable $th) {
            return false;
        }
        return true;
    }

}
