<?php

use App\Http\Controllers\ItemController;
use App\Http\Controllers\QuizController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::middleware('auth:sanctum')->group(function() {
    Route::get('/', function () {
        return redirect(route('show.categories'));
    });
    
    Route::post('/logout', [UserController::class, 'logout'])->name('logout');
    Route::post('/store-token', [UserController::class, 'storeFcmToken'])->name('store.token');

    // first task
    Route::get('/categories', [ItemController::class, 'showcategories'])->name('show.categories');
    Route::get('/items/{category}', [ItemController::class, 'showCategoryItems'])->name('show.category-items');
    Route::match(['get', 'post'], 'create/item/{category}', [ItemController::class, 'createItem'])->name('create.item');
    
    // second task
    Route::get('/quizzes', [QuizController::class, 'showQuizzes'])->name('show.quizzes');
    Route::match(['get', 'post'], 'create/quiz', [QuizController::class, 'createQuiz'])->name('create.quiz');
    Route::match(['get', 'post'], '/add-result/{quiz}', [QuizController::class, 'addResult'])->name('add.result');

});


Route::match(['get', 'post'],'/login', [UserController::class, 'login'])->name('login');
Route::match(['get', 'post'], '/register', [UserController::class, 'register'])->name('register');
